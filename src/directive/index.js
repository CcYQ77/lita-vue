import { App } from 'vue'
import permission from './permission'

export default {
  install(Vuepp) {
    Vue.directive('permission', permission)
  }
}
