import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/home/index.vue'
import User from '@/views/user/index.vue'
import InfoLayout from '@/views/info/index.vue'
import Zone from '@/views/zone/index.vue'
import EditInfo from '@/views/info/edit-info/index.vue'
import Wallet from '@/views/info/wallet/index.vue'
import OrderList from '@/views/info/order/list/index.vue'
import Order from '@/views/info/order/index.vue'
import OrderDetail from '@/views/info/order/detail/index.vue'
// import NotFound from '@/views/404/index.vue'
import createRouteGuard from './guard'
import { REDIRECT_MAIN, NOT_FOUND_ROUTE } from './routes/base'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/skill/:id',
      name: 'Skill',
      component: () => import('@/views/skill/index.vue'),
      meta: {
        isHiddenFooter: true
      }
    },
    {
      path: '/user/:id',
      name: 'User',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: User
    },
    {
      path: '/zone',
      name: 'Zone',
      component: Zone,
      meta: {
        isHiddenFooter: true,
        requiresAuth: true
      }
    },
    {
      path: '/posts/followers',
      name: 'Posts',
      component: Zone,
      meta: {
        isHiddenFooter: true
      }
    },
    {
      path: '/info',
      name: 'Info',
      redirect: '/info/edit-info',
      component: InfoLayout,
      children: [
        {
          path: 'edit-info',
          name: 'EditInfo',
          component: EditInfo,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: 'wallet',
          name: 'Wallet',
          component: Wallet,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: 'follow',
          name: 'Follow',
          component: () => import('@/views/info/follow/index.vue'),
          meta: {
            requiresAuth: true
          }
        },
        {
          path: 'order',
          name: 'Order',
          redirect: '/info/order/list',
          component: Order,
          children: [
            {
              path: 'list',
              name: 'OrderList',
              component: OrderList,
              meta: {
                requiresAuth: true
              }
            },
            {
              path: ':id',
              name: 'OrderDetail',
              component: OrderDetail,
              meta: {
                requiresAuth: true
              }
            }
          ],
          meta: {
            requiresAuth: true
          }
        }
      ],
      meta: {
        isHiddenFooter: true
      }
    },
    REDIRECT_MAIN,
    NOT_FOUND_ROUTE
    // {
    //   path: '/404',
    //   name: 'NotFound',
    //   component: NotFound
    // },
    // {
    //   path: '/:pathMatch(.*)',
    //   redirect: '/404'
    // }
  ],
  scrollBehavior() {
    return { top: 0 }
  }
})
createRouteGuard(router)
export default router
