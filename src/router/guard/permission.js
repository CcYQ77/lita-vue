import usePermission from '@/hooks/permission'
import { useUserStore, useAppStore } from '@/store'
import { appRoutes } from '../routes'
import { WHITE_LIST, NOT_FOUND } from '../constants'
import mitt from '@/utils/mitt'
import { Message } from '@arco-design/web-vue'

export default function setupPermissionGuard(router) {
  router.beforeEach(async (to, from, next) => {
    const appStore = useAppStore()
    const userStore = useUserStore()
    const Permission = usePermission()
    const permissionsAllow = Permission.accessRouter(to)
    if (permissionsAllow) next()
    else {
      if (userStore.token) {
        next()
      } else {
        mitt.emit('setLoginModal', true)
        Message.info('请先登录')
        next({ ...from })
      }
    }
  })
}
