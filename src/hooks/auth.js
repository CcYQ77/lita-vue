import { computed } from 'vue'
import { storeToRefs } from 'pinia'
import { useRoute } from 'vue-router'
import { useUserStore } from '@/store'
export default function useAuth() {
  const route = useRoute()
  const userStore = useUserStore()
  const { role, userInfo } = storeToRefs(userStore)
  const userId = computed(() => Number(route.params.id))
  const isMe = computed(() => userInfo.value.user_id === userId.value)
  const isAdmin = computed(() => role.value === 'Admin')
  const isUser = computed(() => role.value === 'User')
  const isPaidCompanion = computed(() => role.value === 'PaidCompanion' || isAdmin.value)
  return { isMe, isAdmin, isPaidCompanion, isUser, userId }
}
