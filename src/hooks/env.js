const useEnv = () => {
  const { VITE_API_BASE_URL, VITE_PUBLIC_PATH, MODE } = import.meta.env
  // 如果名字变换了，我们可以在这里解构别名
  return {
    MODE,
    VITE_API_BASE_URL,
    VITE_PUBLIC_PATH
  }
}
export default useEnv
