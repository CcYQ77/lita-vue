import request from '@/utils/request'

export const getOrderList = (params) => {
  return request.get('/api/app/order/list', params)
}
export const getBuyOrderList = (params) => {
  return request.get('/api/app/order/buy_list', params)
}
export const getOrderDetail = (id) => {
  return request.get('/api/app/order/detail', { id })
}
export const createOrder = (order) => {
  return request.post('/api/app/order/create', order)
}
export const finishOrder = (id) => {
  return request.get('/api/app/order/finish', { id })
}
export const cancelOrder = (id) => {
  return request.get('/api/app/order/cancel', { id })
}
export const applyCancelOrder = (id) => {
  return request.get('/api/app/order/apply_cancel', { id })
}
