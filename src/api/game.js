import request from '@/utils/request'

export const getAllGame = () => {
  return request.get('/api/app/getAllGame')
}
export const getOneGame = (id) => {
  return request.get('/api/app/getOneGame', { id })
}

export const createSkill = (data) => {
  return request.post('/api/app/createUserSkill', data)
}
export const getSkills = (id) => {
  return request.get('/api/app/getUserSkills', { id })
}
export const conmmentSkill = (data) => {
  return request.post('/api/app/comment_skill', data)
}
export const getUserSkillsCommentList = (data) => {
  return request.get('/api/app/getUserSkillsCommentList', data)
}
