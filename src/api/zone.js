import request from '@/utils/request'

export const getZoneList = (data) => {
  return request.get('/api/zone/getList', data)
}
export const getHotZoneList = (data) => {
  return request.get('/api/zone/getHot', data)
}
export const getFollowList = (data) => {
  return request.get('/api/zone/getFollow', data)
}
export const createZone = (data) => {
  return request.post('/api/zone/create', data)
}
export const updateZone = (data) => {
  return request.put('/api/zone/update', data)
}
export const deleteZone = (id) => {
  return request.delete('/api/zone/delete', { id })
}
export const likeZone = (id) => {
  return request.get('/api/zone/like', { id })
}
export const cancelLikeZone = (id) => {
  return request.get('/api/zone/cancelLike', { id })
}
export const getComments = (id) => {
  return request.get('/api/zone/getComments', { id })
}
export const commentZone = (data) => {
  return request.post('/api/zone/comment', data)
}
export const deleteCommentZone = (data) => {
  return request.delete('/api/zone/deleteComment', data)
}
