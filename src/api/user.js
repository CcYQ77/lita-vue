import request from '@/utils/request'

export const loginByCode = (data) => {
  return request.post('/api/base/loginByCode', data)
}
export const loginByPassword = (data) => {
  return request.post('/api/base/loginByPassword', data)
}
export const getUserInfo = () => {
  return request.get('/api/user/getUserInfo')
}
export const setUserInfo = (info) => {
  return request.put('/api/user/setUserInfo', info)
}
export const getUserInfoById = (id) => {
  return request.get('/api/app/getAppUserInfo', { id })
}
export const logout = () => {
  return request.post('/api/base/logout')
}
export const applyPaidCompanion = () => {
  return request.get('/api/user/applyPaidCompanion')
}
export const getApplyStaus = () => {
  return request.get('/api/user/getApplyStatus')
}
export const getAllGame = () => {
  return request.get('/api/app/getAllGame')
}
export const createSkill = () => {
  return request.post('/api/app/createUserSkill')
}

export const getUserWithSkills = (data) => {
  return request.post('/api/app/getUserListByGameId', data)
}

//获取热门陪玩师
export const getHotUserSkills = () => {
  return request.get('/api/app/getHotUserList')
}
export const followUser = (id) => {
  return request.get('/api/user/follow', { id })
}
export const cancelFollowUser = (id) => {
  return request.get('/api/user/cancelFollow', { id })
}
export const getFollowList = (data) => {
  return request.get('/api/user/getFollowList', data)
}
export const getFansList = (data) => {
  return request.get('/api/user/getFansList', data)
}
export const sendGift = (data) => {
  return request.post('/api/gift/send', data)
}
export const getGifts = (id) => {
  return request.get('/api/gift/getGifts', { id })
}
export const getReceiveGifts = (id) => {
  return request.get('/api/gift/getReceiveGifts', { id })
}
