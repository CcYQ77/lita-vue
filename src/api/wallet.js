import request from '@/utils/request'

export const getAllGoldCoin = () => {
  return request.get('/api/app/getAllGoldCoin')
}
export const addGoldCoin = (id) => {
  return request.get('/api/app/addGoldCoin', { id })
}
export const getGoldCoinRank = (id) => {
  return request.get('/api/app/getGoldCoinRank', { id })
}
