const STATUS_ENUM = {
  OrderStatusing: 1,
  OrderStatusFinish: 2,
  OrderStatusCanceling: 3,
  OrderStatusCancelFinishByBuyer: 4,
  OrderStatusCancelFinishByUser: 5
}
export default STATUS_ENUM
