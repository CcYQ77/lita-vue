import appStore from './modules/app'
import userStore from './modules/user'

export const useAppStore = appStore
export const useUserStore = userStore
