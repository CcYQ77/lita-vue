import { defineStore } from 'pinia'

const useAppStore = defineStore('app', {
  state: () => ({
    count: 0,
    name: 'Lita'
  }),
  getters: {},
  actions: {}
})

export default useAppStore
