/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    spacing: Array.from({ length: 1920 }).reduce((map, _, index) => {
      map[index + 'px'] = `${index}px`
      return map
    }, {}),
    scaleMaps: Array.from({ length: 150 }).reduce((map, _, index) => {
      map[index] = `${index / 100}`
      return map
    }, {}),
    extend: {
      colors: {
        '#333333': '#333333',
        '#666666': '#666666',
        '#999999': '#999999',
        '#F8F8F8': '#F8F8F8',
        '#C2C5C5': '#C2C5C5',
        '#7733FF': 'rgba(119,51,255,1)',
        '#7733ff': 'rgba(119,51,255,1)',
        '#FF4DC4': 'rgba(255,77,196,1)',
        '#525566': 'rgba(82,85,102,1)',
        '#F4F3FF': 'rgba(244,243,255,1)',
        '#E0E6F1': ' rgba(224,230,241,1)',
        '#242633': '#242633',
        '#F5F7F8': 'rgba(245,247,248,1)',
        '#6572AD': '#6572AD',
        '#2D3566': '#2D3566',
        '#838DAC': '#838DAC',
        '#C3C8DF': '#C3C8DF',
        '#FFBB33': '#FFBB33',
        '#16D6B5': '#16D6B5',
        '#FF9B00': '#FF9B00',
        '#8A8C9A': '#8A8C9A',
        '#1CB3FF': '#1CB3FF',
        '#70C6FF': '#70C6FF',
        '#FFE8F5': '#FFE8F5',
        '#FFFFFF': '#FFFFFF',
        '#8A8C99': '#8A8C99',
        '#EFE7FF': '#EFE7FF',
        '#6B39FF': '#6B39FF',
        '#384277': '#384277',
        '#bg01': 'rgba(0, 0, 0, 0.3)',
        '#F5F5F5': '#F5F5F5',
        '#F0F0F0': '#F0F0F0',
        '#f3f3f3': '#f3f3f3',
        '#6b39ff': '#6b39ff'
      },
      spacing: { 0: 0 + 'rem' },
      fontSize: ({ theme }) => ({
        0: 0,
        ...theme('spacing')
      }),

      borderRadius: ({ theme }) => ({
        0: 0,
        '50%': '50%',
        ...theme('spacing')
      }),
      zIndex: {
        1: '1',
        2: '2',
        3: '3'
      },
      scale: ({ theme }) => ({
        0: 0,
        ...theme('scaleMaps')
      })
    }
  },
  plugins: []
}
